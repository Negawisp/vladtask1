package edu.phystech.vladtask1.storage;

import edu.phystech.vladtask1.dto.MessageDTO;

import java.util.LinkedList;
import java.util.List;

public class DataBase {
    private long helloCount = 0;
    private final static String HELLO_FORMAT = "Hello, %s!";
    private List<MessageDTO> messages = new LinkedList<>();

    public void loudAddMessage (MessageDTO mdto) {
        System.out.println(String.format(
                "Got message:\nId: %d\nContent: %s", mdto.getId(), mdto.getContent()));
        messages.add(mdto);
    }

    public MessageDTO loudAddHello (String name) {
        MessageDTO mdto = new MessageDTO(helloCount, String.format(HELLO_FORMAT, name));
        loudAddMessage(mdto);
        return mdto;
    }

    public List<MessageDTO> getMessages () {
        MessageDTO mdto1 = messages.get(0);
        if (mdto1 != null) System.out.println(String.format(
                "Returning 1st message:\nId: %d\nContent: %s", mdto1.getId(), mdto1.getContent()));
        return messages;
    }
}
