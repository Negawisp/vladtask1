package edu.phystech.vladtask1.controllers;

import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
public class VladController {
    public static final String USERNAME_MACRO = "%username%";

    /**
     * Returns a value of <i>number</i> with a mode of <i>mode</i>.
     * @param number
     * @param mode
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public BigInteger getMode (@RequestParam(name="number", defaultValue="0")
                                            BigInteger number,
                               @RequestParam(name="mode", defaultValue="1")
                                            BigInteger mode
                              ) {
        return number.mod(mode);
    }

    /**
     * Replaces all the USERNAME_MACROs in a <i>format</i> with <i>userName</i>.
     * @param userName
     * @param format
     * @return          A modified <i>format</i>.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String modifyString (@RequestParam(name="userName", defaultValue="%username%")
                                        String userName,
                                @RequestBody String format
                               ) {
        return format.replaceAll(USERNAME_MACRO, userName);
    }

    /**
     * Tells which username macro to use.
     * @return  The username macro.
     */
    @RequestMapping(value = "/help")
    public String help () {
        return String.format("Username macro is %s.", USERNAME_MACRO);
    }
}
