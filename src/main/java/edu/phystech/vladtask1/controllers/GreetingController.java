package edu.phystech.vladtask1.controllers;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import edu.phystech.vladtask1.dto.MessageDTO;
import edu.phystech.vladtask1.storage.DataBase;
import org.springframework.web.bind.annotation.*;

//@RestController
public class GreetingController {
    private DataBase mdb = new DataBase();

    @RequestMapping (method = RequestMethod.POST)
    public void addMessage (@RequestBody MessageDTO messageDTO) {
        mdb.loudAddMessage(messageDTO);
    }

    @RequestMapping (value = "/greeting", method = RequestMethod.GET)
    public MessageDTO addGreeting (@RequestParam(value="name", defaultValue="World") String name) {
        return mdb.loudAddHello(name);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<MessageDTO> greeting () {
        return mdb.getMessages();
    }
}
